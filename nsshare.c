/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://mozilla.org/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) 2006 Stephen Deasey <sdeasey@gmail.com>
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU General Public License (the "GPL"), in which case the
 * provisions of GPL are applicable instead of those above.  If you wish
 * to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * License, indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL.
 * If you do not delete the provisions above, a recipient may use your
 * version of this file under either the License or the GPL.
 */

/*
 * nsshare.c --
 *
 *      A compatibility module containing the depreciated AOLserver ns_share
 *      command and a version of ns_set with a -shared flag.
 *
 *       Use nsv_* or ns_cache_* if you can.
 */

#include "share.h"



NS_EXPORT int Ns_ModuleVersion = 1;



/*
 * Static functions defined in this file.
 */

static Ns_TclTraceProc InitInterp;



/*
 *----------------------------------------------------------------------
 *
 * Ns_ModuleInit --
 *
 *      Register the legacy sharing commands.
 *
 * Results:
 *      NS_OK.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

NS_EXPORT int
Ns_ModuleInit(const char *server, const char *module)
{
    Locks *locks;

    if (server == NULL) {
        Ns_Log(Error, "nsshare must be loaded into a virtual server");
        return NS_ERROR;
    }

    locks = ns_malloc(sizeof(Locks));
    Ns_CsInit(&locks->share.cs);
    Ns_MutexSetName2(&locks->share.lock, "nsshare:share", server);
    Ns_CondInit(&locks->share.cond);
    Tcl_InitHashTable(&locks->share.inits, TCL_STRING_KEYS);
    Tcl_InitHashTable(&locks->share.vars, TCL_STRING_KEYS);

    /*
     * Add commands to an initialsing interp.
     */

    Ns_TclRegisterTrace(server, InitInterp, locks, NS_TCL_TRACE_CREATE);

    return NS_OK;
}

static int
InitInterp(Tcl_Interp *interp, const void *arg)
{
    Locks *locks = arg;

    Tcl_CreateObjCommand(interp, "ns_share", ShareObjCmd, locks, NULL);

    return NS_OK;
}


/*
 *----------------------------------------------------------------------
 *
 * Open --
 *
 *      Open a connection to the configured postgres database.
 *
 * Results:
 *      NS_OK or NS_ERROR.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */
